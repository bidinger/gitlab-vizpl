TYPECHECK=poetry run mypy
LINT=poetry run pylint
LINT2=poetry run pycodestyle
BLACK=poetry run black
SRCS=.
LOG_DIR=tmp

## Linting

lint: pylint pycodestyle lint_black typecheck # black

typecheck:
	@echo "Typechecking with mypy version `poetry run mypy --version`"
	$(TYPECHECK) *.py

pylint:
	@echo "Linting with pylint, version:"
	@poetry run pylint --version | sed 's/^/  /'
	$(LINT) $(SRCS)

pycodestyle:
	@echo "Linting with pycodestyle version `poetry run pycodestyle --version` (`poetry run which pycodestyle`)"
	$(LINT2) $(SRCS)

black:
	@echo "Running the black formatter"
	$(BLACK) --line-length 79 $(SRCS)

lint_black:
	@echo "Running black formatter as style checker"
	$(BLACK) --check $(SRCS)

fmt: black


