# pylint: disable=invalid-name
import sys
import gitlab  # type: ignore
from gitlab import Gitlab
from gitlab.v4.objects import Project, ProjectJob, ProjectPipeline  # type: ignore
import json
from collections import defaultdict


def usage(args):
    print(f"Usage: {args[0]} <gitlab-project-id> <gitlab-pipeline-id>")
    print("")
    print(f"E.g.: {args[0]} 9487506 216272777")
    print("")
    print("Project IDs of interest: ")
    print(" nomadiclabs/tezos: 9487506")
    print(" metastatedev/tezos: 11412215")
    print("Requires a gl.cfg file (see gl.cfg.MOVEME)")


def inspect(pipeline):
    jobs = pipeline.jobs.list(all=True)
    names = defaultdict(list)
    for job in jobs:
        if job.stage == 'test':
            names[job.name].append(job)
    for name, val in names.items():
        print(name)
        if len(val) >= 2:
            assert 0

if __name__ == '__main__':
    if len(sys.argv) != 3:
        usage(sys.argv)
        exit(1)
    gl = gitlab.Gitlab.from_config('gitlab', ['gl.cfg'])
    gl.auth()

    project_id = int(sys.argv[1])
    pipeline_id = int(sys.argv[2])

    project = gl.projects.get(project_id, lazy=True)
    pipelines = project.pipelines.list(scope='finished', all=True)
    for pipeline in pipelines:
        inspect(pipeline)
